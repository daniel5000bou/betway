const SomethingFun = () => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <h1>Something fun</h1>
      <img
        style={{ width: 350 }}
        src="https://media.tenor.com/images/c60d560067f1cf8b832f4f653249861e/tenor.gif"
      />
    </div>
  );
};

export default SomethingFun;
