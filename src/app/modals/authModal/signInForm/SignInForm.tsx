import * as Yup from "yup";
import { Formik, Form } from "formik";
import Field from "components/field";
import Button from "components/button";
import axios from "axios";
import { FC } from "react";

export function isEmpty(obj: any) {
  return Object.keys(obj).length !== 0;
}

type Props = {
  handleClose: () => void;
};

const SignSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string()
    .required("Please enter your password")
    .matches(
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])|(?=.*[!@#\$%\^&\*])(?=.{8,})/,
      "Must be at least 8 characters, one uppercase, one number."
    ),
});

const SignInForm: FC<Props> = ({ handleClose }) => {
  return (
    <Formik
      initialValues={{
        password: "",
        email: "",
      }}
      onSubmit={async (values) => {
        try {
          const res = await axios.post("/api/auth", {
            email: values.email,
            password: values.password,
          });
          console.log(`Welcome, ${res.data.name}`);
          handleClose();
        } catch (e) {
          console.log(e);
        }
      }}
      validationSchema={SignSchema}
    >
      {({
        values,
        handleChange,
        handleSubmit,
        touched,
        errors,
        isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <Field
            placeholder="email address"
            label="Email Address"
            name="email"
            error={touched.email && errors.email}
            value={values.email}
            handleChange={handleChange}
          />

          <Field
            placeholder="Password"
            label="Password"
            inputType="password"
            error={touched.password && errors.password}
            value={values.password}
            name="password"
            handleChange={handleChange}
          />

          <Button
            disabled={
              isSubmitting ||
              isEmpty(errors) ||
              values.email === "" ||
              values.password === ""
            }
            buttonType="submit"
          >
            Login
          </Button>
        </Form>
      )}
    </Formik>
  );
};
export default SignInForm;
