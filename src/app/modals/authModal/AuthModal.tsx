import { FC, useState } from "react";
import SignInForm from "./signInForm";
import SignUpForm from "./signUpForm/SignUpForm";
import { AiOutlineClose } from "react-icons/ai";
import styles from "./AuthModal.module.css";
type Props = {
  type: string;
  handleClose: () => void;
};

const AuthModal: FC<Props> = ({ type, handleClose }) => {
  const [state, setState] = useState({
    type: type,
  });

  const toggleTypeSignIn = () => {
    setState({
      type: "sign_in",
    });
  };

  const toggleTypeSignUp = () => {
    setState({
      type: "sign_up",
    });
  };
  return (
    <div className={styles.wrapper}>
      <div>
        {state.type === "sign_in" ? (
          <>
            {" "}
            <h2>Login</h2>
            <span>
              New customer?
              <button
                className={styles.register_link}
                onClick={toggleTypeSignUp}
              >
                Register here
              </button>
            </span>
          </>
        ) : (
          <>
            {" "}
            <h2>SignUp</h2>
            <span>
              Already a customer?
              <button
                className={styles.register_link}
                onClick={toggleTypeSignIn}
              >
                Login here
              </button>
            </span>
          </>
        )}
        <button onClick={handleClose} className={styles.close_wrapper}>
          <AiOutlineClose />
        </button>
      </div>
      <hr className={styles.divider} />
      <div className={styles.form_wrapper}>
        <div className={styles.inner_form_wrapper}>
          {state.type === "sign_in" && <SignInForm handleClose={handleClose} />}
          {state.type === "sign_up" && <SignUpForm />}
        </div>
      </div>

      <span className={styles.forgot_password}>Forgot Username/Password</span>
    </div>
  );
};

export default AuthModal;
