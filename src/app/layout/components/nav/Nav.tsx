import styles from "./Nav.module.css";
import ActiveLink from "components/activeLink";

const Nav = () => {
  return (
    <nav className={styles.nav_wrapper}>
      <style jsx>{`
        .nav-link {
          text-decoration: none;
          text-align: center;
          color: #b8b8b8;
          font-size: 12px;
          flex-grow: 1;
        }
        .active {
          color: white;
          border-bottom: 2px solid#00a826;
          cursor: pointer;
        }
      `}</style>

      <ActiveLink activeClassName="active" href="/">
        <a className="nav-link">
          <p>sport</p>
        </a>
      </ActiveLink>

      <ActiveLink activeClassName="active" href="/casino">
        <a className="nav-link">
          <p>casino</p>{" "}
        </a>
      </ActiveLink>

      <ActiveLink activeClassName="active" href="/live_real">
        <a className="nav-link">
          {" "}
          <p>live & real</p>
        </a>
      </ActiveLink>

      <ActiveLink activeClassName="active" href="/esports">
        <a className="nav-link">
          <p>esports</p>
        </a>
      </ActiveLink>

      <ActiveLink activeClassName="active" href="/vegas">
        <a className="nav-link">
          <p>vegas</p>
        </a>
      </ActiveLink>
    </nav>
  );
};

export default Nav;
