import styles from "./Header.module.css";
import Button from "components/button";
import { FC, useState } from "react";
import Modal from "components/modal";
import AuthModal from "app/modals/authModal";

type Props = {
  logo: string;
};

const Header: FC<Props> = ({ logo }) => {
  const [state, setState] = useState({
    open: false,
    type: "",
  });
  const { open, type } = state;
  const handleClick = (type: string) => {
    setState({
      open: true,
      type,
    });
  };

  const handleClose = () => {
    setState({
      open: false,
      type: "",
    });
  };
  return (
    <>
      <header className={styles.header_wrapper}>
        <img className={styles.logo} src={logo} alt="Betway logo" />
        <div />
        <div className={styles.header_button_inner_wrapper}>
          <Button handleClick={() => handleClick("sign_in")}>Login</Button>
          <Button handleClick={() => handleClick("sign_up")} type="clear">
            Sign up
          </Button>
        </div>
        <div />
      </header>
      {open && (
        <Modal open={open} onClose={handleClose}>
          <AuthModal handleClose={handleClose} type={type} />
        </Modal>
      )}
    </>
  );
};

export default Header;
