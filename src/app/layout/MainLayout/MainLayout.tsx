import { FC, ReactNode } from "react";
import Head from "next/head";
import Nav from "app/layout/components/nav";
import Header from "../components/header";
import Script from "next/script";
import Offer from "components/offer";

type Props = {
  children: ReactNode;
  logo: string;
  background_image: string;
};

const MainLayout: FC<Props> = ({ children, logo, background_image }) => {
  return (
    <>
      <style jsx>{`
        .main {
          display: flex;
          height: calc(100vh - 93px);
          background-color: white;
          background-image: url(${background_image});
          background-position: center;
          background-repeat: no-repeat;
          background-size: cover;
        }
        .main > section {
          padding: 32px;
        }
      `}</style>
      <Head>
        <title>Betway: Official code test</title>
        <meta
          name="description"
          content="Get your exclusive welcome offer when you join Betway today. Experience pre-game and in-play sports betting markets, the latest casino games and more."
        />
        <Script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: `[{
              "@context": "http://schema.org",
              "@type": "Organization",
              "name": "Betway",
              "url": "https://betway.com/",
              "logo": "https://sports.betway.com/Resources/Betway/images/logos/Sports_Betway_InLine.png",
              "sameAs": ["https://www.facebook.com/betway", "https://twitter.com/betway", "https://www.instagram.com/betwayuk/", "https://www.youtube.com/betway", "https://www.linkedin.com/company/betwaygroup/"]
            }]`,
          }}
        />
        <link
          rel="icon"
          href="https://betway.com/welcome/images/icons/favicon-16x16.png"
        />
      </Head>
      <Header logo={logo} />
      <Nav />
      <main className="main">{children}</main>
      <Offer />
    </>
  );
};

export default MainLayout;
