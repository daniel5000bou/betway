import MainLayout from "app/layout/MainLayout";
import type {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";

const HomePage: NextPage = ({
  logo,
  background_image,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <MainLayout logo={logo} background_image={background_image}>
      <div />
    </MainLayout>
  );
};

export default HomePage;

export const getServerSideProps: GetServerSideProps = async (context) => {
  context.res.setHeader(
    "Cache-Control",
    "public, s-maxage=60, stale-while-revalidate=59"
  );

  return {
    props: {
      logo: "https://betway.com/doc-centre/assets/betway-logo-white-sml.png",
      background_image:
        "https://cdn.betwaygroup.com/medusa-production/1001/the-hunch-mobile.webp",
    },
  };
};
