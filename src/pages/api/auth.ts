import axios from 'axios'
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { email, password } = req.body
  try {
    const sign_up = await axios.post(`https://fn-uks-dev-eng-fe-mock-svc.azurewebsites.net/api/sign-in`, {
      email,
      password
    })
    res.status(200).send(sign_up.data)
  } catch (error: any) {
    res.status(500).send(error.message)
  }
}
