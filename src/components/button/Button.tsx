import { FC, ReactNode } from "react";
import styles from "./Button.module.css";

type Props = {
  children: ReactNode;
  handleClick?:() => void;
  type?: string;
  disabled?: boolean;
  buttonType?: "button" | "submit" | "reset" | undefined;
};

const Button: FC<Props> = ({
  children,
  handleClick,
  type = "primary",
  disabled,
  buttonType,
}) => {
  return (
    <>
      <button
        type={buttonType}
        disabled={disabled}
        onClick={handleClick}
        className={styles[type]}
      >
       <div>{children}</div> 
      </button>
    </>
  );
};

export default Button;
