import styles from "./Offer.module.css";
import Button from "components/button";
import Modal from "components/modal";
import SomethingFun from "app/modals/somethingFun/SomethingFun";
import { useState } from "react";

const Offer = () => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <div className={styles.offer}>
        <section>
          <span>sports new customer offer</span>
          <h2>Get up to £10 in free Bets</h2>
          <Button handleClick={() => setOpen(true)}>Join Now</Button>
        </section>
      </div>
      <Modal open={open} onClose={() => setOpen(false)}>
        <SomethingFun />
      </Modal>
    </>
  );
};

export default Offer;
