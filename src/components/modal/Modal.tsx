import useClickOutside from "app/hooks/useClickOutside";
import React, { FC, useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";
import styles from "./Modal.module.css";
export interface ModalProps {
  open: boolean;
  onClose: () => void;
  children: JSX.Element;
  title?: string;
}

export const Modal: FC<ModalProps> = ({ open, onClose, children, title }) => {
  const contentEl = useRef(null);
  const mainEl = useRef(null);
  const [isBrowser, setIsBrowser] = useState(false);
  useClickOutside(mainEl, onClose);
  useEffect(() => {
    setIsBrowser(true);
  }, []);

  const modalContent = open ? (
    <div ref={contentEl} className={styles.modal_wrapper}>
      <section ref={mainEl} className={styles.modal_section}>
        {children}
      </section>
    </div>
  ) : null;

  if (isBrowser) {
    return createPortal(modalContent, document.getElementById("modal-root")!);
  } else {
    return null;
  }
};

export default Modal;
