import React, { FC } from "react";
import { Field } from "formik";
import styles from "./Field.module.css";
type Props = {
  inputType?: "text" | "email" | "password" | "tel" | "checkbox";
  placeholder?: string;
  label?: string;
  handleChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value?: string;
  error?: string | undefined | false;
  name?: string;
};

const StyledField: FC<Props> = ({
  inputType = "text",
  placeholder,
  handleChange,
  value,
  name,
  label,
  error,
}) => {
  return (
    <>
      <label>{label}</label>

      <Field
        className={styles.field}
        type={inputType}
        placeholder={placeholder}
        onChange={handleChange}
        value={value}
        name={name}
        data-testid="input"
      />
      <div className={styles.input_error}>
        {error && <label>{error}</label>}
      </div>
    </>
  );
};

export default StyledField;
